.PHONY: resume site

site: public/index.html

resume: build/resume.pdf build/resume-en.pdf

public/index.html: src/templates/pages/index.html
	python src/main.py

build/resume.pdf: build/resume.html
	wkhtmltopdf --user-style-sheet build/css/print.css build/resume.html build/resume.pdf

build/resume-en.pdf: build/resume-en.html
	wkhtmltopdf --user-style-sheet build/css/print.css build/resume-en.html build/resume-en.pdf

build/resume.html: src/templates/resume.html
	python main.py

build/resume-en.html: src/templates/resume.html
	python main.py
