SKILLS = [
    {
        'title': 'Lenguajes',
        'items': [
            'Python', 'Rust', 'Java', 'Javascript', 'Lua', 'PHP', 'CSS',
            'HTML', 'XML', 'C#', 'Ruby',
        ],
    },
    {
        'title': 'Almacenamiento y distribución',
        'items': ['PostgreSQL', 'MySQL', 'MongoDB', 'Redis', 'RabbitMQ'],
    },
    {
        'title': 'Frameworks',
        'items': ['Django', 'Flask', 'Vue.js', 'Laravel', 'React', '.Net Core'],
    },
    {
        'title': 'Técnicas',
        'items': ['CI', 'TDD', 'Control de versiones'],
    },
    {
        'title': 'Tecnologías',
        'items': ['docker', 'git', 'letsencrypt', 'nginx', 'nodejs', 'geoserver'],
    },
    {
        'title': 'Administración de servidores',
        'items': ['Linux', 'bash', 'cron', 'systemd'],
    },
]

PROFESSIONAL_EXPERIENCE = [
    {
        'title': 'SysAdmin',
        'title_en': '',
        'description': '''Estoy a cargo del mantenimiento de las instancias de
            geoserver utilizadas para los servicios geográficos de los
            productos, incluyendo la migración a nuevas versiones tanto del
            servidor host como de geoserver.''',
        'description_en': '',
        'company': 'Carto Zone',
        'year': '2019-actualidad',
        'technologies': ['geoserver', 'linux'],
    },
    {
        'title': 'Consultor arqueólogo de software',
        'title_en': 'Consulting software archeologist',
        'description': '''Exploración de los servicios e infraestructura
            existente para determinar cuellos de botella, inconsistencias y
            violaciones de invariantes, así como propuestas de mejora e
            implementación de medidas a futuro. Implementación de feature de
            impacto crítico e implementación de pruebas unitarias e integración
            contínua como parte del ciclo de desarrollo.''',
        'description_en': '''Explored the exising services and infrastructure to
            find bottlenecks, inconsistencies and violation of invariants.
            Implemented quick fixes and long term solutions to exising
            problems.''',
        'company': 'Wiggot',
        'year': 2020,
        'technologies': [
            'ruby',
            'nodejs',
            'elasticsearch',
            'mysql',
            'CI',
        ],
    },
    {
        'title': 'Arquitecto y desarrollador fullstack',
        'title_en': 'Architecht and fullstack developer',
        'description': '''Diseño e implementación de software para portales de
            información geográfica con uso de capas vectoriales y ráster, así
            como generación automática de pdfs y administración de información
            relacionada.''',
        'description_en': '''Design and implementation of geographical
            information systems for managing both vector and raster data and
            automatic pdf generation''',
        'company': 'Carto Zone',
        'year': '2018-actualidad',
        'technologies': [
            'django',
            'python',
            'rust',
            'postgresql',
            'postgis',
            'vue.js',
            'geoserver',
        ],
    },
    {
        'title': 'Arquitecto y desarrollador backend',
        'title_en': 'Software architect and fullstack developer',
        'description': '''Estuve a cargo de la arquitectura e implementación del
            backend de un sistema de rastreo de unidades en tiempo real con
            capacidades de alertas basadas en reglas geográficas o temporales,
            reporteo y planeación.''',
        'description_en': '''Designed the architecture and implemented the
            backend portion of a system for real-time vehicle tracking with the
            capability of notifying based on geographical or time related rules,
            making reports, and trip planning and scheduling.''',
        'company': 'Tracsa',
        'year': 2017,
        'technologies': [
            'python',
            'flask',
            'rabbitmq',
            'mongodb',
            'postgresql',
            'java',
        ],
    },
    {
        'title': 'Arquitecto y desarrollador backend',
        'title_en': 'Architecht and backend developer',
        'description': '''Diseñé e implementé la infraestructura de un sistema
            de orquestación y seguimiento de procesos empresariales, diseñada
            para no estar atada a ningún modelo de negocio en particular, sino
            para ser lo suficientemente general para cualquier compañía.''',
        'description_en': '''Designed and implemented the infraestructure
            for a system that manages the company's enterprise processes. The
            system is designied to adapt to any model of operation and it is not
            tied to the company it was built for.''',
        'company': 'Tracsa',
        'year': 2017,
        'technologies': [
            'python',
            'rabbitmq',
            'mongodb',
            'flask',
            'java',
        ],
    },
    {
        'title': 'Desarrollador fullstack',
        'title_en': 'Fullstack developer',
        'description': '''Estuve a cargo de implementar la nueva versión del api
            web de una plataforma para el monitoreo de agentes de campo en
            tiempo real''',
        'description_en': '''I was in charge of implementing the web api of a
            platform for real-time field-agent monitoring.''',
        'company': 'Auronix',
        'year': 2014,
        'technologies': [
            'php',
            'knockoutjs',
            'laravel',
        ],
    },
]

PERSONAL_PROJECTS = [
    {
        'title': 'Pizarra',
        'link': 'https://pizarra.categulario.xyz',
        'description': '''Aplicación de dibujo con zoom y lienzo infinitos, con
            versión para web en webassembly y versión para escritorio en
            gtk.''',
        'description_en': '''A simple drawing application with infinite zoom and
            canvas. Implemented both for web and desktop.''',
        'technologies': [
            'rust',
            'gtk',
            'webassembly',
            'javascript',
        ],
    },
    {
        'title': 'Map matching',
        'link': 'https://github.com/categulario/map_matching',
        'description': '''Una implementación en python de un algoritmo que trata
            de encontrar, dada una traza GPS, las calles por las que debe haber
            pasado el vehículo que la generó.''',
        'description_en': '''Python implementation of an algorithm for finding
            the streets that most likely traveled a vehicle that generated a
            given GPS track.''',
        'technologies': [
            'python',
            'rust',
            'redis',
            'math',
        ],
    },
    {
        'title': 'Karel',
        'link': 'http://karel.categulario.xyz',
        'description': '''Implementación web de un lenguaje orientado a la
            enseñanza de la programación de una forma visual e interactiva.''',
        'description_en': '''Web implementation of a programming language
            designied for teaching programming using a visual and interactive
            environment.''',
        'technologies': [
            'javascript',
            'vuejs',
        ],
    },
    {
        'title': 'Eqxbot',
        'link': 'https://t.me/eqxbot',
        'description': '''Un bot de telegram escrito en rust que renderiza
            fórmulas matemáticas a partir de código LaTex''',
        'description_en': '''A telegram bot that converts formulae in LaTex code
            to images.''',
        'technologies': [
            'rust',
            'actix-web',
        ],
    }
]

TRANSLATIONS = {
    'Acerca de': 'About',
    'Administración de servidores': 'Server administration',
    'Almacenamiento y distribución': 'Storage and messaging',
    'Consultor Arqueólogo de Software': 'Consulting Software Arqueologist',
    'Este sitio no se come tus galletas': 'This site does not eat your cookies',
    'Experiencia profesional': 'Professional experience',
    'Frameworks': 'Frameworks',
    'Habilidades': 'Skills',
    'Lenguajes': 'Programming languages',
    'Pasatiempos': 'Hobbies',
    'Proyectos personales': 'Personal projects',
    'Tecnologías': 'Technologies',
    'Técnicas': 'Techniques',
    'Ubicación': 'Location',
    'Versión actualizada disponible en': 'Updated version available at',
}
