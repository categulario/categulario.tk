#!/usr/bin/env python3
from os import path, listdir
from shutil import copytree
from typing import Dict, Any
from pathlib import Path
import argparse
import json

from jinja2 import Environment, FileSystemLoader, select_autoescape
from jinja2.exceptions import TemplateNotFound
from pydantic import BaseSettings

SOURCE_DIRECTORY = path.dirname(__file__)


class Config(BaseSettings):
    PUBLIC_DIRECTORY: str = path.normpath(path.join(SOURCE_DIRECTORY, '../public'))
    DATA_FILE: str = path.normpath(path.join(SOURCE_DIRECTORY, 'data.json'))


def read_data(path: Path) -> Dict[str, Any]:
    with open(path) as datafile:
        return json.load(datafile)


def build(config: Config):
    context = read_data(config.DATA_FILE)
    env = Environment(
        loader=FileSystemLoader(path.join(SOURCE_DIRECTORY, 'templates')),
        autoescape=select_autoescape()
    )

    # render the pages
    templates = listdir(path.join(SOURCE_DIRECTORY, 'templates/pages'))
    templates = filter(lambda x: x.endswith('.html'), templates)

    for template in templates:
        index = env.get_template(f"pages/{template}")

        with open(path.join(config.PUBLIC_DIRECTORY, template), 'w') as indexfile:
            indexfile.write(index.render(**context))

    # copy static files
    copytree(
        path.join(SOURCE_DIRECTORY, 'static'),
        path.join(config.PUBLIC_DIRECTORY, 'static'),
        dirs_exist_ok=True,
    )

    print(f'Built for production at {config.PUBLIC_DIRECTORY}')


def development_server(config: Config):
    context = read_data(config.DATA_FILE)
    from flask import Flask, render_template

    app = Flask(__name__)

    @app.route("/")
    def index():
        return render_template('pages/index.html', **context)

    @app.route("/<path>")
    def pages(path):
        try:
            return render_template(f'pages/{path}', **context)
        except TemplateNotFound:
            return f'Template {path} not found', 404

    app.run(host='127.0.0.1', port=8000, debug=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build an static site')

    parser.add_argument(
        '--dev', action='store_true', help='run the development server',
    )

    args = parser.parse_args()

    if args.dev:
        development_server(Config())
    else:
        build(Config())
